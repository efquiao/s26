const http = require("http");

//Create a variable "port" to store the port number
const port = 4000;

//Create a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {
  //Accessing the 'greeting' route that returns a message of "Hello Again"
  //http://localhost:4000/greeting
  //request is an object is an object that is sent via the client
  if (request.url == "/greeting") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcom to the login page");
  } //Mini Activity:
  //Access the "homepage" route and return a message of "This is the homepage" with a status code 200 and a plain text.
  //All other routes
 else {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.end("I'm sorry the page you are looking for cannot be found.");
  }
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}`);
